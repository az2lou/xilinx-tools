#!/bin/bash

XILINX_TOOL_HOME="${XILINX_TOOL_HOME:-/home/az2lou/Code/xilinx-tools}"

alias vivado="/opt/Xilinx/Vivado/2018.2/bin/vivado"

for device in xc7s50 xc7s75 xc7s100
do
    FOLDERS="/home/az2lou/Code/xilinx-tools/output/$device/*"
    echo "design, logic, rent parameter, fmax (MHz)" > $XILINX_TOOL_HOME/output/$device/results.csv

    for f in $FOLDERS
    do
        design=$(basename $f)
        echo $design

        cd $f

        # Rent parameter
        p_rent=$(grep "template |" design.txt | tr -s ' ' | cut -d ' ' -f 6)

        # LUTs used
        luts=$(grep 'Slice LUTs.\s*\|\s*(\d+)' utilization.txt | tr -s ' ' | cut -d ' ' -f 5)

        # FMax
        # From Vivado
        wns=$(grep -A 6 "Intra Clock Table" proj/proj.runs/impl_1/template_timing_summary_routed.rpt | tail -n 1 | tr -s " " | cut -d" " -f2)
        # 100 Mhz target clock
        period=10

        fmax=$(echo "1000 / ($period - $wns)" | bc)

        echo "$design, $luts, $p_rent, $fmax" >> $XILINX_TOOL_HOME/output/$device/results.csv
    done
done
