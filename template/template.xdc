create_clock -period 10.000 -waveform {0.000 5.000} [get_nets clk]
create_clock -period 10.000 -waveform {0.000 5.000} [get_nets clock]
create_clock -period 10.000 -waveform {0.000 5.000} [get_nets tm3_clk_v0] # stereovision

# don't use DSP blocks
set_property use_dsp no [get_cells -hierarchical -filter { IS_PRIMITIVE == "FALSE" }]
