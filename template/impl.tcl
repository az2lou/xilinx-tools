create_project -force proj ./proj -part xc7z020clg400-1
add_files [glob *.v]
read_xdc template.xdc

update_compile_order -fileset sources_1
set_property top template [current_fileset]

launch_runs synth_1 -jobs 4
wait_on_run synth_1

launch_runs impl_1 -jobs 4
wait_on_run impl_1

if { [ catch {open_run impl_1 -name impl_1} fid] } {
  puts stderr "Could not open project\n$fid"
  exit 1
}
report_utilization -file utilization.txt
report_design_analysis -complexity -file design.txt
report_timing -file timing.txt
close_project
exit

# report for impl
# open_project proj/proj.xpr

# if { [ catch {open_run impl_1 -name impl_1} fid] } {
#   puts stderr "Could not open project\n$fid"
#   exit 1
# }

# report_utilization -file utilization.txt
# report_design_analysis -complexity -file design.txt
# report_timing -file timing.txt
# close_project
# exit
