#!/bin/bash

XILINX_TOOL_HOME="${XILINX_TOOL_HOME:-/home/az2lou/Code/xilinx-tools}"

FILES="/home/az2lou/Code/efinix-tools/verilog/*"

alias vivado="/opt/Xilinx/Vivado/2018.2/bin/vivado"

for f in $FILES
do
    filename="$(basename -s .v $f)"

    echo $filename

    for device in xc7s50 xc7s75 xc7s100
    do
        # create the directory
        mkdir -p $XILINX_TOOL_HOME/output/$device/$filename

        # copy the template project
        cp -r $XILINX_TOOL_HOME/template/* $XILINX_TOOL_HOME/output/$device/$filename

        # copy the verilog file
        cp $f $XILINX_TOOL_HOME/output/$device/$filename/template.v

        # rename the top-level module (done manually already)

        cd $XILINX_TOOL_HOME/output/$device/$filename/

        # change the device in the tcl script
        awk -v device="$device" '/create_project/{$NF=device} 1' impl.tcl > tmp
        mv tmp impl.tcl

        /opt/Xilinx/Vivado/2018.2/bin/vivado -mode tcl -s impl.tcl

    done

    # print newline
    echo
done
